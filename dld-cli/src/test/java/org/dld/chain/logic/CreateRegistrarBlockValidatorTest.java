package org.dld.chain.logic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dld.app.core.ErrorCodes;
import org.dld.app.data.DldDataStore;
import org.dld.app.json.JacksonConfig;
import org.dld.app.util.CryptoUtil;
import org.dld.chain.block.Block;
import org.dld.chain.block.CreateRegistrar;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

@SpringBootTest(classes = {JacksonConfig.class, CreateRegistrarBlockValidator.class, DldDataStore.class})
class CreateRegistrarBlockValidatorTest {

    private static PrivateKey general1PrivateKey;
    private static PublicKey general1PublicKey;
    private static PublicKey general2PublicKey;
    private static PrivateKey sreg001PrivateKey;
    private static PublicKey sreg001PublicKey;
    private static PublicKey reg001PublicKey;
    private static PrivateKey genesis001PrivateKey;
    private static PublicKey genesis001PublicKey;
    private static PrivateKey reg001PrivateKey;

    @BeforeAll
    static void loadKeys() throws NoSuchAlgorithmException, IOException {
        general1PrivateKey = CryptoUtil.loadPrivateKey(new ClassPathResource("keypairs/general-001/dld.key").getFile())
                .orElseThrow();
        general1PublicKey = CryptoUtil.loadPublicKey(new ClassPathResource("keypairs/general-001/dld.pub").getFile())
                .orElseThrow();
        general2PublicKey = CryptoUtil.loadPublicKey(new ClassPathResource("keypairs/general-002/dld.pub").getFile())
                .orElseThrow();

        sreg001PrivateKey = CryptoUtil.loadPrivateKey(new ClassPathResource("keypairs/sreg-001/dld.key").getFile())
                .orElseThrow();
        sreg001PublicKey = CryptoUtil.loadPublicKey(new ClassPathResource("keypairs/sreg-001/dld.pub").getFile())
                .orElseThrow();
        genesis001PrivateKey =
                CryptoUtil.loadPrivateKey(new ClassPathResource("keypairs/genesis-001/dld.key").getFile())
                .orElseThrow();
        genesis001PublicKey = CryptoUtil.loadPublicKey(new ClassPathResource("keypairs/genesis-001/dld.pub").getFile())
                .orElseThrow();
        reg001PublicKey = CryptoUtil.loadPublicKey(new ClassPathResource("keypairs/reg-001/dld.pub").getFile())
                .orElseThrow();
        reg001PrivateKey = CryptoUtil.loadPrivateKey(new ClassPathResource("keypairs/reg-001/dld.key").getFile())
                .orElseThrow();
    }

    @Nested
    @SpringBootTest(classes = {JacksonConfig.class, CreateRegistrarBlockValidator.class, DldDataStore.class})
    class NewChain {

        private final DldDataStore dldDataStore;
        private final CreateRegistrarBlockValidator blockValidator;
        private final ObjectMapper objectMapper;

        @Autowired
        NewChain(final ObjectMapper objectMapper) throws IOException {
            this.dldDataStore = new DldDataStore(objectMapper);
            this.blockValidator = new CreateRegistrarBlockValidator(dldDataStore, objectMapper);
            this.objectMapper = objectMapper;

            dldDataStore.loadDldChain(new ClassPathResource("chains/genesis.chain").getFile());
        }

        @Test
        @DisplayName("It should throw an exception if the registrar id is missing")
        void registrarIdMissing() throws JsonProcessingException, NoSuchAlgorithmException {
            CreateRegistrar createRegistrar = new CreateRegistrar()
                    .setParentBlockSignature(dldDataStore.getHeadBlockSignature())
                    .setSuperRegistrar(true)
                    .setRegistrarPublicKey(CryptoUtil.encodePublicKeyToString(general2PublicKey))
                    .setExpires(null);

            String signedPayload = CryptoUtil.sign(objectMapper.writeValueAsString(createRegistrar), genesis001PrivateKey)
                    .orElseThrow();

            InvalidBlockException exception = Assertions.assertThrows(InvalidBlockException.class,
                    () -> blockValidator.validate(createRegistrar, signedPayload, "genesis-001", genesis001PublicKey));
            Assertions.assertEquals(ErrorCodes.ENTITY_ID_MUST_BE_SPECIFIED, exception.getErrorCode());
        }


        @Test
        @DisplayName("It should throw an exception if the registrar public key is missing")
        void registrarPublicKeyMissing() throws JsonProcessingException, NoSuchAlgorithmException {
            CreateRegistrar createRegistrar = new CreateRegistrar()
                    .setParentBlockSignature(dldDataStore.getHeadBlockSignature())
                    .setSuperRegistrar(true)
                    .setRegistrarId("sreg-001")
                    .setExpires(null);

            String signedPayload = CryptoUtil.sign(objectMapper.writeValueAsString(createRegistrar), genesis001PrivateKey)
                    .orElseThrow();

            InvalidBlockException exception = Assertions.assertThrows(InvalidBlockException.class,
                    () -> blockValidator.validate(createRegistrar, signedPayload, "genesis-001", genesis001PublicKey));
            Assertions.assertEquals(ErrorCodes.ENTITY_PUBLIC_KEY_MUST_BE_SPECIFIED, exception.getErrorCode());
        }


        @Test
        @DisplayName("It should successfully create a block when the genesis registrar tries to create a super registrar")
        void createSuperRegistrar_genesis() throws JsonProcessingException, NoSuchAlgorithmException {
            CreateRegistrar createRegistrar = new CreateRegistrar()
                    .setParentBlockSignature(dldDataStore.getHeadBlockSignature())
                    .setSuperRegistrar(true)
                    .setRegistrarId("sreg-001")
                    .setRegistrarPublicKey(CryptoUtil.encodePublicKeyToString(sreg001PublicKey))
                    .setExpires(null);

            String signedPayload = CryptoUtil.sign(objectMapper.writeValueAsString(createRegistrar), genesis001PrivateKey)
                    .orElseThrow();

            Block result = Assertions.assertDoesNotThrow(
                    () -> blockValidator.validate(createRegistrar, signedPayload, "genesis-001", genesis001PublicKey));
            Assertions.assertNotNull(result);
        }
    }

    @Nested
    @SpringBootTest(classes = {JacksonConfig.class, CreateRegistrarBlockValidator.class, DldDataStore.class})
    class SuperRegistrarInChain {

        private final CreateRegistrarBlockValidator blockValidator;
        private final ObjectMapper objectMapper;
        private final DldDataStore dldDataStore;

        @Autowired
        SuperRegistrarInChain(final ObjectMapper objectMapper) throws IOException {
            this.dldDataStore = new DldDataStore(objectMapper);
            this.blockValidator = new CreateRegistrarBlockValidator(dldDataStore, objectMapper);
            this.objectMapper = objectMapper;

            dldDataStore.loadDldChain(new ClassPathResource("chains/sreg001.chain").getFile());
        }

        @Test
        @DisplayName("It should throw an exception if a non-genesis registrar tries to create a super registrar")
        void createSuperRegistrar_nonGenesis() throws JsonProcessingException, NoSuchAlgorithmException {
            CreateRegistrar createRegistrar = new CreateRegistrar()
                    .setParentBlockSignature(dldDataStore.getHeadBlockSignature())
                    .setSuperRegistrar(true)
                    .setRegistrarId("sreg-002")
                    .setRegistrarPublicKey(CryptoUtil.encodePublicKeyToString(general2PublicKey))
                    .setExpires(null);

            String signedPayload = CryptoUtil.sign(objectMapper.writeValueAsString(createRegistrar), sreg001PrivateKey)
                    .orElseThrow();

            InvalidBlockException exception = Assertions.assertThrows(InvalidBlockException.class,
                    () -> blockValidator.validate(createRegistrar, signedPayload, "sreg-001", sreg001PublicKey));
            Assertions.assertEquals(ErrorCodes.REGISTRAR_NOT_GENESIS, exception.getErrorCode());
        }


        @Test
        @DisplayName("It should throw an exception if the registrar writing the record is not in the chain")
        void writingRegistrarMissing() throws JsonProcessingException, NoSuchAlgorithmException {
            CreateRegistrar createRegistrar = new CreateRegistrar()
                    .setParentBlockSignature(dldDataStore.getHeadBlockSignature())
                    .setSuperRegistrar(false)
                    .setRegistrarId("reg-001")
                    .setRegistrarPublicKey(CryptoUtil.encodePublicKeyToString(reg001PublicKey))
                    .setExpires(null);

            String signedPayload = CryptoUtil.sign(objectMapper.writeValueAsString(createRegistrar), general1PrivateKey)
                    .orElseThrow();

            InvalidBlockException exception = Assertions.assertThrows(InvalidBlockException.class,
                    () -> blockValidator.validate(createRegistrar, signedPayload, "general-001", general1PublicKey));
            Assertions.assertEquals(ErrorCodes.REGISTRAR_DOES_NOT_EXIST, exception.getErrorCode());
        }

        @Test
        @DisplayName("It should successfully create a block when a non-genesis super registrar tries to create a registrar")
        void createRegistrar_superRegistrar() throws JsonProcessingException, NoSuchAlgorithmException {
            CreateRegistrar createRegistrar = new CreateRegistrar()
                    .setParentBlockSignature(dldDataStore.getHeadBlockSignature())
                    .setSuperRegistrar(false)
                    .setRegistrarId("reg-001")
                    .setRegistrarPublicKey(CryptoUtil.encodePublicKeyToString(reg001PublicKey))
                    .setExpires(null);

            String signedPayload = CryptoUtil.sign(objectMapper.writeValueAsString(createRegistrar), sreg001PrivateKey)
                    .orElseThrow();

            Block result = Assertions.assertDoesNotThrow(
                    () -> blockValidator.validate(createRegistrar, signedPayload, "sreg-001", sreg001PublicKey));
            Assertions.assertNotNull(result);
        }
    }

    @Nested
    @SpringBootTest(classes = {JacksonConfig.class, CreateRegistrarBlockValidator.class, DldDataStore.class})
    class SuperRegistrarAndNormalRegistrarInChain {

        private final CreateRegistrarBlockValidator blockValidator;
        private final ObjectMapper objectMapper;
        private final DldDataStore dldDataStore;

        @Autowired
        SuperRegistrarAndNormalRegistrarInChain(final ObjectMapper objectMapper) throws IOException {
            this.dldDataStore = new DldDataStore(objectMapper);
            this.blockValidator = new CreateRegistrarBlockValidator(dldDataStore, objectMapper);
            this.objectMapper = objectMapper;

            dldDataStore.loadDldChain(new ClassPathResource("chains/reg001.chain").getFile());
        }

        @Test
        @DisplayName("It should throw an exception if a non-super registrar tries to create a registrar")
        void createRegistrar_nonSuperRegistrar() throws JsonProcessingException, NoSuchAlgorithmException {
            CreateRegistrar createRegistrar = new CreateRegistrar()
                    .setParentBlockSignature(dldDataStore.getHeadBlockSignature())
                    .setSuperRegistrar(false)
                    .setRegistrarId("reg-002")
                    .setRegistrarPublicKey(CryptoUtil.encodePublicKeyToString(general2PublicKey))
                    .setExpires(null);

            String signedPayload = CryptoUtil.sign(objectMapper.writeValueAsString(createRegistrar), reg001PrivateKey)
                    .orElseThrow();

            InvalidBlockException exception = Assertions.assertThrows(InvalidBlockException.class,
                    () -> blockValidator.validate(createRegistrar, signedPayload, "reg-001", reg001PublicKey));
            Assertions.assertEquals(ErrorCodes.REGISTRAR_NOT_SUPER, exception.getErrorCode());
        }
    }
}
