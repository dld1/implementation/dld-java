package org.dld.chain.logic;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.dld.app.core.ErrorCodes;
import org.dld.app.data.DldDataStore;
import org.dld.app.json.JacksonConfig;
import org.dld.app.util.CryptoUtil;
import org.dld.chain.block.Block;
import org.dld.chain.block.BlockTypes;
import org.dld.chain.block.Genesis;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.ObjectId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

@SpringBootTest(classes = {JacksonConfig.class, GenesisBlockValidator.class, DldDataStore.class})
class GenesisBlockValidatorTest {

    private static PrivateKey general1PrivateKey;
    private static PublicKey general1PublicKey;
    private static PublicKey general2PublicKey;

    private final GenesisBlockValidator genesisBlockValidator;
    private final ObjectMapper objectMapper;
    private Git git;

    @Autowired
    GenesisBlockValidatorTest(final GenesisBlockValidator genesisBlockValidator, final ObjectMapper objectMapper) {
        this.genesisBlockValidator = genesisBlockValidator;
        this.objectMapper = objectMapper;
    }

    @BeforeAll
    static void loadKeys() throws NoSuchAlgorithmException, IOException {
        general1PrivateKey = CryptoUtil.loadPrivateKey(new ClassPathResource("keypairs/general-001/dld.key").getFile())
                .orElseThrow();
        general1PublicKey = CryptoUtil.loadPublicKey(new ClassPathResource("keypairs/general-001/dld.pub").getFile())
                .orElseThrow();
        general2PublicKey = CryptoUtil.loadPublicKey(new ClassPathResource("keypairs/general-002/dld.pub").getFile())
                .orElseThrow();
    }

    @BeforeEach
    void createTemporaryEmptyGitRepo() throws IOException, GitAPIException {
        File tempDirectory = Files.createTempDirectory("dld-tests-temp").toFile();

        git = Git.init().setDirectory(tempDirectory).call();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                FileUtils.deleteDirectory(tempDirectory);

            } catch (IOException e) {
                System.err.println("Unable to delete temporary directory: " + e.getLocalizedMessage());
            }
        }));
    }

    @Test
    @DisplayName("It should throw an error when the repository does not exist")
    void repoDoesNotExist() {
        Genesis genesis = new Genesis().setGenesisSuperRegistrarId("testId")
                .setGenesisSuperRegistrarPublicKey(CryptoUtil.encodePublicKeyToString(general1PublicKey))
                .setLegislationRepositoryMainlineBranchName("main")
                .setLegislationRepositoryMainlineHeadHash("fixme")
                .setSpecVersion("v2")
                .setLegislationRepositorySourceURI("fakerepo/.git");

        InvalidBlockException exception = Assertions.assertThrows(InvalidBlockException.class,
                () -> genesisBlockValidator.validate(genesis));
        Assertions.assertEquals(ErrorCodes.GIT_REPO_NOT_FOUND, exception.getErrorCode());
    }

    @Test
    @DisplayName("It should throw an error if the mainline branch does not exist")
    void repoMainlineBranchDoesNotExist() {
        Genesis genesis = new Genesis().setGenesisSuperRegistrarId("testId")
                .setGenesisSuperRegistrarPublicKey(CryptoUtil.encodePublicKeyToString(general1PublicKey))
                .setLegislationRepositoryMainlineBranchName("main")
                .setLegislationRepositoryMainlineHeadHash("fixme")
                .setSpecVersion("v2")
                .setLegislationRepositorySourceURI(git.getRepository().getDirectory().getAbsolutePath());

        InvalidBlockException exception = Assertions.assertThrows(InvalidBlockException.class,
                () -> genesisBlockValidator.validate(genesis));
        Assertions.assertEquals(ErrorCodes.GIT_MAINLINE_REF_NOT_FOUND, exception.getErrorCode());
    }

    @Test
    @DisplayName("It should throw an error if the mainline branch's head ref hash does not match the value in the " +
            "block")
    void repoMainlineHeadHashDoesNotMatch() throws IOException, GitAPIException {
        // Add a file to the repository to create an initial commit
        File testFile = new File(git.getRepository().getDirectory().getParent(), "testfile");
        testFile.createNewFile();

        git.add().addFilepattern("testfile").call();
        git.commit().setMessage("Initial commit").call();

        Genesis genesis = new Genesis().setGenesisSuperRegistrarId("testId")
                .setGenesisSuperRegistrarPublicKey(CryptoUtil.encodePublicKeyToString(general1PublicKey))
                .setLegislationRepositoryMainlineBranchName("master")
                .setLegislationRepositoryMainlineHeadHash("fixme")
                .setSpecVersion("v2")
                .setLegislationRepositorySourceURI(git.getRepository().getDirectory().getAbsolutePath());
        InvalidBlockException exception = Assertions.assertThrows(InvalidBlockException.class,
                () -> genesisBlockValidator.validate(genesis));
        Assertions.assertEquals(ErrorCodes.GIT_HASH_DOES_NOT_MATCH, exception.getErrorCode());
    }

    @Test
    @DisplayName("It should throw an error if the block's public key does not match the writing key")
    void writerPublicKeyDoesNotMatch() throws IOException, GitAPIException, NoSuchAlgorithmException {
        // Add a file to the repository to create an initial commit
        File testFile = new File(git.getRepository().getDirectory().getParent(), "testfile");
        testFile.createNewFile();

        git.add().addFilepattern("testfile").call();
        git.commit().setMessage("Initial commit").call();
        ObjectId lastCommitId = git.getRepository().resolve(Constants.HEAD);

        Genesis genesis = new Genesis().setGenesisSuperRegistrarId("testId")
                .setGenesisSuperRegistrarPublicKey(CryptoUtil.encodePublicKeyToString(general1PublicKey))
                .setLegislationRepositoryMainlineBranchName("master")
                .setLegislationRepositoryMainlineHeadHash(lastCommitId.getName())
                .setSpecVersion("v2")
                .setLegislationRepositorySourceURI(git.getRepository().getDirectory().getAbsolutePath());

        String signedPayload = CryptoUtil.sign(objectMapper.writeValueAsString(genesis), general1PrivateKey)
                .orElseThrow();

        InvalidBlockException exception = Assertions.assertThrows(InvalidBlockException.class,
                () -> genesisBlockValidator.validate(genesis, signedPayload, "testId", general2PublicKey));
        Assertions.assertEquals(ErrorCodes.INVALID_SIGNATURE, exception.getErrorCode());
    }

    @Test
    @DisplayName("It should validate successfully when everything is in order")
    void validBlock() throws IOException, GitAPIException, NoSuchAlgorithmException {
        // Add a file to the repository to create an initial commit
        File testFile = new File(git.getRepository().getDirectory().getParent(), "testfile");
        testFile.createNewFile();

        git.add().addFilepattern("testfile").call();
        git.commit().setMessage("Initial commit").call();
        ObjectId lastCommitId = git.getRepository().resolve(Constants.HEAD);

        Genesis genesis = new Genesis().setGenesisSuperRegistrarId("testId")
                .setGenesisSuperRegistrarPublicKey(CryptoUtil.encodePublicKeyToString(general1PublicKey))
                .setLegislationRepositoryMainlineBranchName("master")
                .setLegislationRepositoryMainlineHeadHash(lastCommitId.getName())
                .setSpecVersion("v2")
                .setLegislationRepositorySourceURI(git.getRepository().getDirectory().getAbsolutePath());

        String signedPayload = CryptoUtil.sign(objectMapper.writeValueAsString(genesis), general1PrivateKey)
                .orElseThrow();

        Block validBlock = Assertions.assertDoesNotThrow(() -> genesisBlockValidator.validate(genesis,
                signedPayload, "testId", general1PublicKey));
        Assertions.assertAll(() -> Assertions.assertEquals(BlockTypes.GENESIS, validBlock.getPayload().getType()),
                () -> Assertions.assertEquals(CryptoUtil.encodePublicKeyToString(general1PublicKey),
                        ((Genesis) validBlock.getPayload()).getGenesisSuperRegistrarPublicKey()),
                () -> Assertions.assertEquals("master",
                        ((Genesis) validBlock.getPayload()).getLegislationRepositoryMainlineBranchName()),
                () -> Assertions.assertEquals(lastCommitId.getName(),
                        ((Genesis) validBlock.getPayload()).getLegislationRepositoryMainlineHeadHash()),
                () -> Assertions.assertEquals("v2",
                        ((Genesis) validBlock.getPayload()).getSpecVersion()),
                () -> Assertions.assertEquals(git.getRepository().getDirectory().getAbsolutePath(),
                        ((Genesis) validBlock.getPayload()).getLegislationRepositorySourceURI())
        );
    }
}
