package org.dld.app.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

class CryptoUtilTest {

    private static PrivateKey general1PrivateKey;
    private static PublicKey general1PublicKey;
    private static PrivateKey general2PrivateKey;
    private static PublicKey general2PublicKey;
    private static File tempDirectory;

    private final String general1PublicKeyEncoded =
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA9k03pSRE8rPE9LfMh0Z+Q4D1+/Ndq5cNHGwWeow3xkcMtyiAk/stD0zVFd" +
                    "yMXzhEu8CnImRvas0FQywEzrjAS6niuQYjV0dNsOSKUQA9HaGhyc3hwY8CzjRDy5KYCsKK6l8VDQvUv5V5vrtwrPtV2OHr" +
                    "7Ng2g/8MOKzdRtqbTZkUaqACoUImva7xwXpfRGH/+Hy9NG/srt+3TD/n9YMzMKQrGOOVs4B5aht5hBKIfVFOG2tRirarGd" +
                    "YYFLpb8bpkARMYKMHceFsBv/d9ijkkMh6M3G5MFuK+XcQVD3A5V2AfyfDAm+JoDjQDqvrko8V14NVKXlKJ0P1vuGQHtoRf" +
                    "TQIDAQAB";

    @BeforeAll
    static void createTempDir() throws IOException {
        tempDirectory = Files.createTempDirectory("dld-tests-temp").toFile();
        tempDirectory.deleteOnExit();
    }

    @BeforeAll
    static void loadKeys() throws NoSuchAlgorithmException, IOException {
        general1PrivateKey = CryptoUtil.loadPrivateKey(new ClassPathResource("keypairs/general-001/dld.key").getFile())
                .orElseThrow();
        general1PublicKey = CryptoUtil.loadPublicKey(new ClassPathResource("keypairs/general-001/dld.pub").getFile())
                .orElseThrow();
        general2PrivateKey = CryptoUtil.loadPrivateKey(new ClassPathResource("keypairs/general-002/dld.key").getFile())
                .orElseThrow();
        general2PublicKey = CryptoUtil.loadPublicKey(new ClassPathResource("keypairs/general-002/dld.pub").getFile())
                .orElseThrow();
    }

    @Test
    @DisplayName("It should generate a valid keypair")
    void generateKeyPair() {
        Assertions.assertDoesNotThrow(() -> {
            KeyPair keyPair = CryptoUtil.generateKeyPair();
            Assertions.assertTrue(CryptoUtil.doesKeyPairMatch(keyPair.getPrivate(), keyPair.getPublic()),
                    "Generated key pair does not match");
        });
    }

    @Nested
    class DoesKeyPairMatch {

        @Test
        @DisplayName("It should return true with a valid key pair")
        void matchingPair() {
            Assertions.assertDoesNotThrow(() -> Assertions.assertTrue(CryptoUtil
                    .doesKeyPairMatch(general1PrivateKey, general1PublicKey)));
        }

        @Test
        @DisplayName("It should return false with an invalid key pair")
        void nonMatchingPair() {
            Assertions.assertDoesNotThrow(() -> Assertions.assertFalse(CryptoUtil
                    .doesKeyPairMatch(general2PrivateKey, general1PublicKey)));
        }
    }

    @Nested
    class LoadPrivateKey {

        @Test
        @DisplayName("It should successfully load the general-001 private key")
        void loadGeneral1Key() throws NoSuchAlgorithmException {
            PrivateKey privateKey = Assertions.assertDoesNotThrow(() -> CryptoUtil.loadPrivateKey(new ClassPathResource("keypairs/general-001" +
                    "/dld.key").getFile())).orElse(null);

            Assertions.assertNotNull(privateKey);
            Assertions.assertTrue(CryptoUtil.doesKeyPairMatch(privateKey, general1PublicKey));
            Assertions.assertFalse(CryptoUtil.doesKeyPairMatch(privateKey, general2PublicKey));
        }

        @Test
        @DisplayName("It should successfully load the general-002 private key")
        void loadGeneral2Key() throws NoSuchAlgorithmException {
            PrivateKey privateKey =
                    Assertions.assertDoesNotThrow(() -> CryptoUtil.loadPrivateKey(new ClassPathResource("keypairs/general-002" +
                    "/dld.key").getFile())).orElse(null);

            Assertions.assertNotNull(privateKey);
            Assertions.assertTrue(CryptoUtil.doesKeyPairMatch(privateKey, general2PublicKey));
            Assertions.assertFalse(CryptoUtil.doesKeyPairMatch(privateKey, general1PublicKey));
        }

        @Test
        @DisplayName("It should fail to load the general-001 public key as a private key")
        void loadGeneral1PublicKey() {
            PrivateKey privateKey =
                    Assertions.assertDoesNotThrow(() -> CryptoUtil.loadPrivateKey(new ClassPathResource("keypairs/general-001" +
                    "/dld.pub").getFile())).orElse(null);

            Assertions.assertNull(privateKey);
        }
    }

    @Nested
    class LoadPublicKey {

        @Test
        @DisplayName("It should successfully load the general-001 public key")
        void loadGeneral1Key() throws NoSuchAlgorithmException {
            PublicKey publicKey = Assertions.assertDoesNotThrow(() -> CryptoUtil.loadPublicKey(new ClassPathResource(
                    "keypairs/general-001/dld.pub").getFile())).orElse(null);

            Assertions.assertNotNull(publicKey);
            Assertions.assertTrue(CryptoUtil.doesKeyPairMatch(general1PrivateKey, publicKey));
            Assertions.assertFalse(CryptoUtil.doesKeyPairMatch(general2PrivateKey, publicKey));
        }

        @Test
        @DisplayName("It should successfully load the general-002 public key")
        void loadGeneral2Key() throws NoSuchAlgorithmException {
            PublicKey publicKey = Assertions.assertDoesNotThrow(() -> CryptoUtil.loadPublicKey(new ClassPathResource(
                    "keypairs/general-002/dld.pub").getFile())).orElse(null);

            Assertions.assertNotNull(publicKey);
            Assertions.assertTrue(CryptoUtil.doesKeyPairMatch(general2PrivateKey, publicKey));
            Assertions.assertFalse(CryptoUtil.doesKeyPairMatch(general1PrivateKey, publicKey));
        }

        @Test
        @DisplayName("It should fail to load the general-001 private key as a public key")
        void loadGeneral1PrivateKey() {
            PublicKey publicKey = Assertions.assertDoesNotThrow(() -> CryptoUtil.loadPublicKey(new ClassPathResource(
                    "keypairs/general-001/dld.key").getFile())).orElse(null);

            Assertions.assertNull(publicKey);
        }
    }

    @Nested
    class WritePublicKey {

        @Test
        @DisplayName("It should successfully write the public key")
        void writeKey() throws NoSuchAlgorithmException {
            KeyPair keyPair = CryptoUtil.generateKeyPair();

            String keyFilePath = tempDirectory.getAbsolutePath() + "crypto-util-writePublicKey.pub";
            Assertions.assertDoesNotThrow(() -> CryptoUtil.writePublicKey(keyPair.getPublic(),  keyFilePath));

            PublicKey loadedKey = CryptoUtil.loadPublicKey(new File(keyFilePath)).orElse(null);
            Assertions.assertNotNull(loadedKey);
            Assertions.assertTrue(CryptoUtil.doesKeyPairMatch(keyPair.getPrivate(), loadedKey));
        }
    }

    @Nested
    class WritePrivateKey {


        @Test
        @DisplayName("It should successfully write the private key")
        void writeKey() throws NoSuchAlgorithmException {
            KeyPair keyPair = CryptoUtil.generateKeyPair();

            String keyFilePath = tempDirectory.getAbsolutePath() + "crypto-util-writePrivateKey.pub";
            Assertions.assertDoesNotThrow(() -> CryptoUtil.writePrivateKey(keyPair.getPrivate(),  keyFilePath));

            PrivateKey loadedKey = CryptoUtil.loadPrivateKey(new File(keyFilePath)).orElse(null);
            Assertions.assertNotNull(loadedKey);
            Assertions.assertTrue(CryptoUtil.doesKeyPairMatch(loadedKey, keyPair.getPublic()));
        }
    }

    @Nested
    class EncodePublicKeyToString {

        @Test
        @DisplayName("It should encode to the known Base64 representation of general-001's public key")
        void encodeGeneralPublicKey1() {
            String encoded = CryptoUtil.encodePublicKeyToString(general1PublicKey);
            Assertions.assertEquals(general1PublicKeyEncoded, encoded);
        }
    }

    @Nested
    class DecodePublicKeyFromString {

        @Test
        @DisplayName("It should decode the known Base64 representation of general-001's public key to a key that " +
                "matches the associated private key")
        void encodeGeneralPublicKey1() throws NoSuchAlgorithmException {
            PublicKey publicKey = CryptoUtil.decodePublicKeyFromString(general1PublicKeyEncoded).orElse(null);
            Assertions.assertNotNull(publicKey);
            Assertions.assertTrue(CryptoUtil.doesKeyPairMatch(general1PrivateKey, publicKey));
        }
    }

    @Nested
    class SignAndVerify {

        @Test
        @DisplayName("Signing a string with general-001 private key should be verifiable with the public key")
        void validSignatureProduced() throws NoSuchAlgorithmException {
            String signed = CryptoUtil.sign(general1PublicKeyEncoded, general1PrivateKey).orElse(null);
            Assertions.assertNotNull(signed);
            Assertions.assertTrue(CryptoUtil.verifySignature(signed, general1PublicKeyEncoded, general1PublicKey));
            Assertions.assertFalse(CryptoUtil.verifySignature(signed, general1PublicKeyEncoded, general2PublicKey));
        }
    }
}
