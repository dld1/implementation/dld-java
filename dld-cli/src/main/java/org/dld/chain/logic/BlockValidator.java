package org.dld.chain.logic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dld.app.core.ErrorCodes;
import org.dld.app.data.DldDataStore;
import org.dld.app.util.CryptoUtil;
import org.dld.chain.block.Block;
import org.dld.chain.block.BlockPayload;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;

public abstract class BlockValidator<T extends BlockPayload> {

    protected final DldDataStore dldDataStore;
    private final ObjectMapper objectMapper;

    protected BlockValidator(final DldDataStore dldDataStore,
            final ObjectMapper objectMapper) {
        this.dldDataStore = dldDataStore;
        this.objectMapper = objectMapper;
    }

    /**
     * Validate the payload and return a Block.
     *
     * @param payload The payload to be written.
     * @param signature The signature of payload signed by the private key associated with the writing user.
     * @param writerId The id of the user that is writing this block to the chain.
     * @throws InvalidBlockException If the block is not valid.
     * @return The valid Block object
     */
    public Block validate(final T payload, final String signature,
            final String writerId, final PublicKey writerPublicKey) throws InvalidBlockException,
            IOException {
        validateUserCanWrite(payload, writerId);
        validate(payload);
        verifySignature(payload, signature, writerPublicKey);
        return Block.create(payload, signature, writerId);
    }

    private void verifySignature(final T payload, final String encodedSignature, final PublicKey writerPublicKey) throws InvalidBlockException {
        try {
            if (!CryptoUtil.verifySignature(encodedSignature, objectMapper.writeValueAsString(payload),
                    writerPublicKey)) {
                throw new InvalidBlockException(ErrorCodes.INVALID_SIGNATURE);
            }
        } catch (NoSuchAlgorithmException e) {
            throw new InvalidBlockException(ErrorCodes.RSA_NOT_FOUND);
        } catch (JsonProcessingException e) {
            throw new InvalidBlockException(ErrorCodes.JSON_SERIALIZATION_ERROR);
        }
    }

    abstract void validateUserCanWrite(final T payload, final String writerId);

    /**
     * Validates the block payload against the datastore and other sources.
     *
     * @param payload The payload to be validated.
     * @throws InvalidBlockException If the block is not valid.
     */
    abstract void validate(T payload) throws InvalidBlockException;
}
