package org.dld.chain.logic;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.dld.app.core.ErrorCodes;
import org.dld.app.data.DldDataStore;
import org.dld.app.data.types.Registrar;
import org.dld.app.util.CryptoUtil;
import org.dld.chain.block.CreateRegistrar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;

@Component
public class CreateRegistrarBlockValidator extends BlockValidator<CreateRegistrar> {

    @Autowired
    protected CreateRegistrarBlockValidator(final DldDataStore dldDataStore, final ObjectMapper objectMapper) {
        super(dldDataStore, objectMapper);
    }

    @Override
    void validateUserCanWrite(final CreateRegistrar payload, final String writerId) {
        Registrar registrar = dldDataStore.getRegistrarDataStore().retrieve(writerId).orElse(null);
        if (registrar == null) {
            throw new InvalidBlockException(ErrorCodes.REGISTRAR_DOES_NOT_EXIST);
        }

        if (!registrar.isSuperRegistrar()) {
            throw new InvalidBlockException(ErrorCodes.REGISTRAR_NOT_SUPER);
        }

        if (payload.isSuperRegistrar() && !registrar.isGenesis()) {
            throw new InvalidBlockException(ErrorCodes.REGISTRAR_NOT_GENESIS);
        }
    }

    @Override
    void validate(final CreateRegistrar payload) throws InvalidBlockException {

        if (payload.getRegistrarId() == null || payload.getRegistrarId().isEmpty()) {
            throw new InvalidBlockException(ErrorCodes.ENTITY_ID_MUST_BE_SPECIFIED);
        }

        if (dldDataStore.getRegistrarDataStore().contains(payload.getRegistrarId())) {
            throw new InvalidBlockException(ErrorCodes.REGISTRAR_ALREADY_EXISTS);
        }

        if (payload.getExpires() != null && ZonedDateTime.now().isAfter(payload.getExpires())) {
            throw new InvalidBlockException(ErrorCodes.CANNOT_CREATE_EXPIRED_ENTITY);
        }

        CryptoUtil.decodePublicKeyFromString(payload.getRegistrarPublicKey())
                .orElseThrow(() -> new InvalidBlockException(ErrorCodes.ENTITY_PUBLIC_KEY_MUST_BE_SPECIFIED));
    }

}
