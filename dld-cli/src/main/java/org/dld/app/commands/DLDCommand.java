package org.dld.app.commands;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dld.app.commands.handlers.PrivateKeyConverter;
import org.dld.app.data.DldDataStore;
import org.dld.app.util.CryptoUtil;
import org.dld.chain.block.BlockPayload;
import org.dld.chain.block.CreateRegistrar;
import org.dld.chain.block.Genesis;
import org.dld.chain.block.UpdateRegistrar;
import picocli.CommandLine;

import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Optional;
import java.util.concurrent.Callable;

public abstract class DLDCommand implements Callable<Integer> {

    @CommandLine.Option(names = { "--privatekey", "-k" }, required = true, converter = PrivateKeyConverter.class,
            description = "Path to the private key file of the user that is performing this operation")
    protected PrivateKey privateKey;

    @CommandLine.Option(names = { "--chainfile", "-f" }, defaultValue = "dld.chain", description = "File name for the DLD chain")
    protected String blockChainFileName;

    @CommandLine.Option(names = { "--userid", "-u" }, required = true, description = "Id of the user that is " +
            "performing this operation")
    protected String userId;


    protected final ObjectMapper jsonMapper;

    protected final DldDataStore dldDataStore;

    protected DLDCommand(final ObjectMapper objectMapper, final DldDataStore dldDataStore) {
        this.jsonMapper = objectMapper;
        this.dldDataStore = dldDataStore;
    }

    protected Optional<String> signPayload(final BlockPayload payload, final PrivateKey writerPrivateKey) {
        try {
            return CryptoUtil.sign(jsonMapper.writeValueAsString(payload), writerPrivateKey);
        } catch (NoSuchAlgorithmException e) {
            System.err.println("SHA256 with RSA Signature Algorithm not found.");
        } catch (JsonProcessingException e) {
            System.err.println("Could not serialize block: " + e.getLocalizedMessage());
        }
        return Optional.empty();
    }


    protected <T extends BlockPayload> Optional<PublicKey> determinePublicKey(final T payload, final String writerId) {
        switch (payload.getType()) {
            case GENESIS -> {
                return CryptoUtil.decodePublicKeyFromString(((Genesis) payload).getGenesisSuperRegistrarPublicKey());
            }

            case CREATE_REGISTRAR, UPDATE_REGISTRAR -> {
                if ((payload instanceof CreateRegistrar && ((CreateRegistrar) payload).isSuperRegistrar())
                        || (payload instanceof UpdateRegistrar && ((UpdateRegistrar) payload).isSuperRegistrar())) {
                    return Optional.of(dldDataStore.getRegistrarDataStore().getGenesisSuperRegistrarPublicKey());
                }

                return dldDataStore.getRegistrarDataStore().getRegistrarPublicKey(writerId);
            }
            case CREATE_VOTER, UPDATE_VOTER, CREATE_TABULATOR, UPDATE_TABULATOR -> {
                return dldDataStore.getRegistrarDataStore().getRegistrarPublicKey(writerId);
            }

            case PROPOSE_LEGISLATION, DELEGATE_VOTE, VOTE -> {
                return dldDataStore.getVoterDataStore().getVoterPublicKey(writerId);
            }
            case VOTE_TALLY, VOTE_RESULT -> {
                return dldDataStore.getTabulatorDataStore().getTabulatorPublicKey(writerId);
            }
            case UPDATE_SPEC_VERSION -> {
                return Optional.of(dldDataStore.getRegistrarDataStore().getGenesisSuperRegistrarPublicKey());
            }
        }

        return Optional.empty();
    }

}
