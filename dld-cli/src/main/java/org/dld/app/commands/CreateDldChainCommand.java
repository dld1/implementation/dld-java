package org.dld.app.commands;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.dld.app.commands.handlers.PublicKeyConverter;
import org.dld.app.core.ErrorCodes;
import org.dld.app.data.DldDataStore;
import org.dld.app.util.CryptoUtil;
import org.dld.chain.block.Block;
import org.dld.chain.block.Genesis;
import org.dld.chain.logic.GenesisBlockValidator;
import org.dld.chain.logic.InvalidBlockException;
import org.springframework.beans.factory.annotation.Autowired;
import picocli.CommandLine;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;

@CommandLine.Command(name = "create-chain", mixinStandardHelpOptions = true)
public class CreateDldChainCommand extends DLDCommand {

    @CommandLine.Option(names = { "--publickey", "-p" }, required = true, converter = PublicKeyConverter.class,
            description  = "Path to the super registrar public key. Must be the public key for the private key used in this operation")
    private PublicKey publicKey;

    @CommandLine.Option(names = { "--gitrepo", "-g" }, required = true, description = "URI to the git repository that will base this chain")
    private String gitRepoURI; // TODO Support remote repositories

    @CommandLine.Option(names = { "--mainbranch", "-m" }, required = true, description = "Name of the main branch on the repository")
    private String mainBranchName;

    @CommandLine.Option(names = { "--headhash", "-c" }, required = true, description = "Hash of the HEAD commit on the repository's mainline branch")
    private String mainBranchHeadCommitHash;

    @CommandLine.Option(names = { "--specversion", "-s" }, required = true, description = "Version of the spec to which this chain is compliant")
    private String specVersion;

    private final GenesisBlockValidator genesisBlockValidator;

    @Autowired
    CreateDldChainCommand(final DldDataStore dldDataStore, final ObjectMapper jsonMapper, final GenesisBlockValidator genesisBlockValidator) {
        super(jsonMapper, dldDataStore);
        this.genesisBlockValidator = genesisBlockValidator;
    }

    @Override
    public Integer call() {

        try {
            if (!CryptoUtil.doesKeyPairMatch(privateKey, publicKey)) {
                System.err.println("Public and private keys do not match.");
                return ErrorCodes.KEY_PAIR_MISMATCH.getCode();
            }
        } catch (NoSuchAlgorithmException e) {
            System.err.println("RSA Algorithm not found.");
            return ErrorCodes.RSA_NOT_FOUND.getCode();
        }

        this.dldDataStore.setDldChainFile(new File(blockChainFileName));

        Genesis genesisPayload = new Genesis().setLegislationRepositorySourceURI(gitRepoURI)
                .setLegislationRepositoryMainlineBranchName(mainBranchName)
                .setLegislationRepositoryMainlineHeadHash(mainBranchHeadCommitHash)
                .setGenesisSuperRegistrarPublicKey(CryptoUtil.encodePublicKeyToString(publicKey))
                .setGenesisSuperRegistrarId(userId)
                .setSpecVersion("v2");

        String signedPayload = signPayload(genesisPayload, privateKey).orElse(null);
        if (signedPayload == null) {
            System.err.println("Payload signing failed.");
            return ErrorCodes.UNABLE_TO_SIGN_PAYLOAD.getCode();
        }

        File dldChainFile = new File(blockChainFileName);
        if (dldChainFile.exists()) {
            System.err.println("Cannot create DLD chain. A file with that name already exists.");
            return ErrorCodes.NEW_CHAIN_FILE_EXISTS.getCode();
        }

        try {
            PublicKey writerPublicKey = determinePublicKey(genesisPayload, userId).orElse(null);
            if (writerPublicKey == null) {
                return ErrorCodes.WRITER_PUBLIC_KEY_NOT_FOUND.getCode();
            }
            Block block = genesisBlockValidator.validate(genesisPayload, signedPayload, userId, writerPublicKey);
            dldDataStore.writeBlock(block);
        } catch (IOException e) {
            System.err.println("An error occurred while writing the DLD Chain: " + e.getLocalizedMessage());
            return ErrorCodes.FILE_IO_ERROR.getCode();
        } catch (InvalidBlockException e) {
            return e.getErrorCode().getCode();
        }

        return 0;
    }
}
