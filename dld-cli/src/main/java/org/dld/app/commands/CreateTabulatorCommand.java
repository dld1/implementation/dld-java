package org.dld.app.commands;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.dld.app.commands.handlers.PublicKeyConverter;
import org.dld.app.core.ErrorCodes;
import org.dld.app.data.BlockDecodingException;
import org.dld.app.data.DldDataStore;
import org.dld.app.data.types.Registrar;
import org.dld.app.util.CryptoUtil;
import org.dld.chain.block.Block;
import org.dld.chain.block.CreateTabulator;
import org.dld.chain.logic.CreateTabulatorBlockValidator;
import org.springframework.beans.factory.annotation.Autowired;
import picocli.CommandLine;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.PublicKey;
import java.time.ZonedDateTime;
import java.util.concurrent.Callable;

@CommandLine.Command(name = "create-tabulator", mixinStandardHelpOptions = true)
public class CreateTabulatorCommand extends DLDCommand implements Callable<Integer> {

    @CommandLine.Option(names = { "--publickey", "-p" }, required = true, converter = PublicKeyConverter.class,
            description = "Path to the public key file to be used for this operation")
    private PublicKey publicKey;

    @CommandLine.Option(names = { "--tabulatorid", "--id" }, required = true, description = "The ID to use for the " +
            "tabulator")
    private String tabulatorId;

    @CommandLine.Option(names = { "--expiry", "-e" }, description = "Optional expiry date-time for a voter " +
            "(ISO-8601 zoned date time)")
    private ZonedDateTime expiry;

    private final CreateTabulatorBlockValidator createTabulatorBlockValidator;

    @Autowired
    protected CreateTabulatorCommand(final ObjectMapper objectMapper,
            final DldDataStore dldDataStore, final CreateTabulatorBlockValidator createTabulatorBlockValidator) {
        super(objectMapper, dldDataStore);
        this.createTabulatorBlockValidator = createTabulatorBlockValidator;
    }

    @Override
    public Integer call() throws Exception {
        try {
            dldDataStore.loadDldChain(new File(blockChainFileName));
        } catch (IOException e) {
            System.err.println("An IOException occurred while trying to read from the chain file." + e.getLocalizedMessage());
            return ErrorCodes.FILE_IO_ERROR.getCode();
        } catch (BlockDecodingException e) {
            System.err.println("Block chain decoding failed: " + e.getLocalizedMessage());
            return ErrorCodes.BLOCK_CHAIN_DECODE_FAILED.getCode();
        }

        Registrar registrar = dldDataStore.getRegistrarDataStore().retrieve(userId).orElse(null);
        if (registrar == null) {
            return ErrorCodes.REGISTRAR_DOES_NOT_EXIST.getCode();
        }

        if (!CryptoUtil.doesKeyPairMatch(privateKey, registrar.getPublicKey())) {
            return ErrorCodes.KEY_PAIR_MISMATCH.getCode();
        }

        CreateTabulator createTabulator = new CreateTabulator()
                .setTabulatorId(tabulatorId)
                .setTabulatorPublicKey(CryptoUtil.encodePublicKeyToString(publicKey))
                .setExpires(expiry)
                .setParentBlockSignature(dldDataStore.getHeadBlockSignature());

        String signedPayload = signPayload(createTabulator, privateKey).orElse(null);
        if (signedPayload == null) {
            System.err.println("Payload signing failed.");
            return ErrorCodes.UNABLE_TO_SIGN_PAYLOAD.getCode();
        }

        PublicKey writerPublicKey = determinePublicKey(createTabulator, userId).orElse(null);
        if (writerPublicKey == null) {
            return ErrorCodes.WRITER_PUBLIC_KEY_NOT_FOUND.getCode();
        }

        Block createTabulatorBlock = createTabulatorBlockValidator.validate(createTabulator, signedPayload, userId,
                writerPublicKey);

        File dldChainFile = new File(blockChainFileName);

        try (FileWriter writer = new FileWriter(dldChainFile, true)) {
            writer.append(jsonMapper.writeValueAsString(createTabulatorBlock));
            writer.append('\n');
        } catch (IOException e) {
            System.err.println("An error occurred while writing the DLD Chain: " + e.getLocalizedMessage());
            return ErrorCodes.FILE_IO_ERROR.getCode();
        }

        return 0;
    }
}
