package org.dld.app.commands;

import org.dld.app.core.ErrorCodes;
import org.dld.app.util.CryptoUtil;
import org.springframework.stereotype.Component;
import picocli.CommandLine;

import java.io.IOException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.Callable;

@Component
@CommandLine.Command(name = "genkeys", mixinStandardHelpOptions = true)
public class GenerateKeysCommand implements Callable<Integer> {

    @Override
    public Integer call() {
        System.out.println("Generating keys...");
        KeyPair keyPair;
        try {
            keyPair = CryptoUtil.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            System.err.println("RSA Algorithm not found.");
            return ErrorCodes.RSA_NOT_FOUND.getCode();
        }

        String outFile = "dld";
        try {
            CryptoUtil.writePrivateKey(keyPair.getPrivate(), outFile + ".key");
            CryptoUtil.writePublicKey(keyPair.getPublic(), outFile + ".pub");
        } catch (IOException e) {
            System.err.println(e.getLocalizedMessage());
            return ErrorCodes.FILE_IO_ERROR.getCode();
        }

        System.out.println("Key generation complete.");
        return 0;
    }
}
