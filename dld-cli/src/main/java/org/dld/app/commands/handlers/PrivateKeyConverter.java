package org.dld.app.commands.handlers;

import org.dld.app.commands.exceptions.UnableToLoadPrivateKeyException;
import org.dld.app.util.CryptoUtil;
import picocli.CommandLine;

import java.io.File;
import java.security.PrivateKey;

public class PrivateKeyConverter implements CommandLine.ITypeConverter<PrivateKey> {
    @Override
    public PrivateKey convert(final String fileName) throws Exception {
        PrivateKey privateKey = CryptoUtil.loadPrivateKey(new File(fileName))
                .orElse(null);

        if (privateKey == null) {
            throw new UnableToLoadPrivateKeyException();
        }

        return privateKey;
    }
}
