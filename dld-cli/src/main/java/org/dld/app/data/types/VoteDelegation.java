package org.dld.app.data.types;

import org.dld.chain.block.DelegateVote;

import java.time.ZonedDateTime;
import java.util.Objects;

public class VoteDelegation {

    private final String delegatorId;
    private final String delegateeId;
    private final ZonedDateTime expires;

    private VoteDelegation(final String delegatorId, final String delegateeId, final ZonedDateTime expires) {
        Objects.requireNonNull(delegatorId);
        Objects.requireNonNull(delegateeId);
        this.delegatorId = delegatorId;
        this.delegateeId = delegateeId;
        this.expires = expires;
    }

    public String getDelegatorId() {
        return delegatorId;
    }

    public String getDelegateeId() {
        return delegateeId;
    }

    public ZonedDateTime getExpires() {
        return expires;
    }

    @Override
    public String toString() {
        return "VoteDelegation{" +
                "delegatorId='" + delegatorId + '\'' +
                ", delegateeId='" + delegateeId + '\'' +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        VoteDelegation that = (VoteDelegation) o;
        return delegatorId.equals(that.delegatorId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(delegatorId);
    }

    public static VoteDelegation create(final DelegateVote delegateVote) {
        return new VoteDelegation(delegateVote.getDelegatorId(), delegateVote.getDelegateId(),
                delegateVote.getDelegationEndTime());
    }
}
