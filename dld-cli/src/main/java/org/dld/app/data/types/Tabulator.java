package org.dld.app.data.types;

import org.dld.app.data.BlockDecodingException;
import org.dld.app.util.CryptoUtil;
import org.dld.chain.block.CreateTabulator;
import org.dld.chain.block.UpdateTabulator;

import java.security.PublicKey;
import java.time.ZonedDateTime;

public class Tabulator {
    private final String id;
    private final PublicKey publicKey;
    private final ZonedDateTime expires;

    private Tabulator(final String id, final PublicKey publicKey, final ZonedDateTime expires) {
        this.id = id;
        this.publicKey = publicKey;
        this.expires = expires;
    }

    public String getId() {
        return id;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public ZonedDateTime getExpires() {
        return expires;
    }

    @Override
    public String toString() {
        return "Tabulator{" +
                "id='" + id + '\'' +
                ", publicKey='" + publicKey + '\'' +
                ", expires=" + expires +
                '}';
    }

    public static Tabulator create(final CreateTabulator createTabulator) {
        return new Tabulator(createTabulator.getTabulatorId(),
                CryptoUtil.decodePublicKeyFromString(createTabulator.getTabulatorPublicKey())
                        .orElseThrow(() -> new BlockDecodingException("Unable to decode tabulator's public key")),
                createTabulator.getExpires());
    }

    public static Tabulator create(final UpdateTabulator updateTabulator) {
        return new Tabulator(updateTabulator.getTabulatorId(),
                CryptoUtil.decodePublicKeyFromString(updateTabulator.getTabulatorPublicKey())
                        .orElseThrow(() -> new BlockDecodingException("Unable to decode tabulator's public key")),
                updateTabulator.getExpires());
    }
}
