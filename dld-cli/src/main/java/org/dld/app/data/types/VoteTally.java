package org.dld.app.data.types;

import org.dld.chain.block.VoteTallyPayload;

import java.util.Objects;

public class VoteTally {

    private final String tabulatorId;
    private final String proposalSignature;
    private final long totalVotes;
    private final long totalDirectVotes;
    private final long totalInFavour;
    private final long totalOpposed;
    private final boolean proposalPassed;
    private final String inFavourChecksum;
    private final String opposedChecksum;

    private VoteTally(final VoteTallyPayload voteTallyPayload) {
        this.tabulatorId = voteTallyPayload.getTabulatorId();
        this.proposalSignature = voteTallyPayload.getProposalSignature();
        this.totalVotes = voteTallyPayload.getTotalVotes();
        this.totalDirectVotes = voteTallyPayload.getTotalDirectVotes();
        this.totalInFavour = voteTallyPayload.getTotalInFavour();
        this.totalOpposed = voteTallyPayload.getTotalOpposed();
        this.proposalPassed = voteTallyPayload.isProposalPassed();
        this.inFavourChecksum = voteTallyPayload.getInFavourChecksum();
        this.opposedChecksum = voteTallyPayload.getOpposedChecksum();
    }

    public String getTabulatorId() {
        return tabulatorId;
    }

    public String getProposalSignature() {
        return proposalSignature;
    }

    public long getTotalVotes() {
        return totalVotes;
    }

    public long getTotalDirectVotes() {
        return totalDirectVotes;
    }

    public long getTotalInFavour() {
        return totalInFavour;
    }

    public long getTotalOpposed() {
        return totalOpposed;
    }

    public boolean isProposalPassed() {
        return proposalPassed;
    }

    public String getInFavourChecksum() {
        return inFavourChecksum;
    }

    public String getOpposedChecksum() {
        return opposedChecksum;
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        VoteTally voteTally = (VoteTally) other;
        return Objects.equals(tabulatorId, voteTally.tabulatorId) && Objects.equals(proposalSignature, voteTally.proposalSignature);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tabulatorId, proposalSignature);
    }

    @Override
    public String toString() {
        return "VoteTally{" +
                "tabulatorId='" + tabulatorId + '\'' +
                ", proposalSignature='" + proposalSignature + '\'' +
                ", totalVotes=" + totalVotes +
                ", totalDirectVotes=" + totalDirectVotes +
                ", totalInFavour=" + totalInFavour +
                ", totalOpposed=" + totalOpposed +
                ", proposalPassed=" + proposalPassed +
                ", inFavourChecksum='" + inFavourChecksum + '\'' +
                ", opposedChecksum='" + opposedChecksum + '\'' +
                '}';
    }

    public static VoteTally create(final VoteTallyPayload voteTallyPayload) {
        return new VoteTally(voteTallyPayload);
    }
}
