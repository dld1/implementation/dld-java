package org.dld.app.data.types;

import org.dld.chain.block.ProposeLegislation;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

public class LegislationProposal {

    private final String legislationProposalSourceURI;
    private final String legislationProposalBranchName;
    private final String legislationProposalHeadHash;
    private final ZonedDateTime voteOpenTime;
    private final ZonedDateTime voteCloseTime;
    private final String legislationProposalSignature;

    private final Set<VoteDelegation> generalDelegations;
    private final Set<Vote> votes;
    private final Set<VoteTally> voteTallies;
    private VoteResult voteResult;

    public LegislationProposal(final String legislationProposalSourceURI,
            final String legislationProposalBranchName, final String legislationProposalHeadHash,
            final ZonedDateTime voteOpenTime, final ZonedDateTime voteCloseTime,
            final String proposalSignature) {
        this.legislationProposalSourceURI = legislationProposalSourceURI;
        this.legislationProposalBranchName = legislationProposalBranchName;
        this.legislationProposalHeadHash = legislationProposalHeadHash;
        this.voteOpenTime = voteOpenTime;
        this.voteCloseTime = voteCloseTime;
        this.legislationProposalSignature = proposalSignature;
        this.generalDelegations = new HashSet<>();
        this.votes = new HashSet<>();
        this.voteTallies = new HashSet<>();
    }

    public String getLegislationProposalSourceURI() {
        return legislationProposalSourceURI;
    }

    public String getLegislationProposalBranchName() {
        return legislationProposalBranchName;
    }

    public String getLegislationProposalHeadHash() {
        return legislationProposalHeadHash;
    }

    public ZonedDateTime getVoteOpenTime() {
        return voteOpenTime;
    }

    public ZonedDateTime getVoteCloseTime() {
        return voteCloseTime;
    }

    public String getLegislationProposalSignature() {
        return legislationProposalSignature;
    }

    public void setVoteResult(final VoteResult voteResult) {
        if (this.voteResult == null) {
            throw new IllegalStateException("Cannot set the vote result twice on a block");
        }

        this.voteResult = voteResult;
    }

    public void add(final VoteDelegation voteDelegation) {
        this.generalDelegations.add(voteDelegation);
    }

    public void add(final Vote vote) {
        this.votes.add(vote);
    }

    public void add(final VoteTally voteTally) {
        this.voteTallies.add(voteTally);
    }

    @Override
    public String toString() {
        return "LegislationProposal{" +
                "legislationProposalSourceURI='" + legislationProposalSourceURI + '\'' +
                ", legislationProposalBranchName='" + legislationProposalBranchName + '\'' +
                ", legislationProposalHeadHash='" + legislationProposalHeadHash + '\'' +
                ", voteOpenTime=" + voteOpenTime +
                ", voteCloseTime=" + voteCloseTime +
                ", legislationProposalSignature='" + legislationProposalSignature + '\'' +
                ", generalDelegations=" + generalDelegations +
                '}';
    }

    public static LegislationProposal create(final ProposeLegislation proposeLegislation,
            final String proposalSignature) {
        return new LegislationProposal(proposeLegislation.getLegislationProposalSourceURI(),
                proposeLegislation.getLegislationProposalBranchName(),
                proposeLegislation.getLegislationProposalHeadHash(), proposeLegislation.getVoteOpenTime(),
                proposeLegislation.getVoteCloseTime(), proposalSignature);
    }
}
