package org.dld.app.data;

public class BlockDecodingException extends RuntimeException {

    public BlockDecodingException(final String message) {
        super(message);
    }
}
