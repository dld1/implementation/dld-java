package org.dld.app.data;

import org.dld.app.data.types.Tabulator;
import org.dld.app.data.types.Voter;
import org.dld.chain.block.CreateTabulator;
import org.dld.chain.block.UpdateTabulator;

import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class TabulatorDataStore {

    private final Map<String, Tabulator> tabulatorMap;

    public TabulatorDataStore() {
        tabulatorMap = new HashMap<>();
    }

    /**
     * Retrieve the tabulator associated with the provided id.
     *
     * @param id The id of the tabulator to be retrieved.
     * @return An Optional containing the tabulator if it exists in the store.
     */
    public Optional<Tabulator> retrieve(final String id) {
        return Optional.ofNullable(tabulatorMap.get(id));
    }

    /**
     * Determine whether a tabulator exists with the provided id.
     *
     * @param id The tabulator id.
     * @return True if the data store contains a tabulator with the given id.
     */
    public boolean contains(final String id) {
        return tabulatorMap.containsKey(id);
    }

    /**
     * Retrieve the public key associated with the tabulator with the given id.
     *
     * @param tabulatorId The tabulator's id.
     * @return An Optional containing the most recent public key associated with the id parameter if found.
     */
    public Optional<PublicKey> getTabulatorPublicKey(final String tabulatorId) {
        return Optional.ofNullable(tabulatorMap.get(tabulatorId)).map(Tabulator::getPublicKey);
    }

    void handle(final CreateTabulator createTabulator) {
        Tabulator tabulator = Tabulator.create(createTabulator);
        tabulatorMap.put(tabulator.getId(), tabulator);
    }

    void handle(final UpdateTabulator updateTabulator) {
        Tabulator tabulator = Tabulator.create(updateTabulator);
        tabulatorMap.put(tabulator.getId(), tabulator);
    }
}
