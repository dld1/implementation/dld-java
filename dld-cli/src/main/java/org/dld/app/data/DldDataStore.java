package org.dld.app.data;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dld.app.core.ErrorCodes;
import org.dld.chain.block.Block;
import org.dld.chain.block.CreateRegistrar;
import org.dld.chain.block.CreateTabulator;
import org.dld.chain.block.CreateVoter;
import org.dld.chain.block.DelegateVote;
import org.dld.chain.block.Genesis;
import org.dld.chain.block.ProposeLegislation;
import org.dld.chain.block.UpdateRegistrar;
import org.dld.chain.block.UpdateSpecVersion;
import org.dld.chain.block.UpdateTabulator;
import org.dld.chain.block.UpdateVoter;
import org.dld.chain.block.VotePayload;
import org.dld.chain.block.VoteResultPayload;
import org.dld.chain.block.VoteTallyPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

@Component
public class DldDataStore {

    private final ObjectMapper objectMapper;
    private final ConfigurationDataStore configurationDataStore;
    private final RegistrarDataStore registrarDataStore;
    private final TabulatorDataStore tabulatorDataStore;
    private final VoterDataStore voterDataStore;
    private final DemocracyDataStore democracyDataStore;

    private String headBlockSignature;
    private File dldChainFile;

    @Autowired
    public DldDataStore(final ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
        this.configurationDataStore = new ConfigurationDataStore();
        this.registrarDataStore = new RegistrarDataStore();
        this.tabulatorDataStore = new TabulatorDataStore();
        this.voterDataStore = new VoterDataStore();
        this.democracyDataStore = new DemocracyDataStore();
    }

    public void setDldChainFile(final File dldChainFile) {
        if (this.dldChainFile != null) {
            System.out.println("Block chain already loaded - nothing to do.");
            return;
        }
        this.dldChainFile = dldChainFile;
    }

    /**
     * Load the chain into memory.
     *
     * @param dldChainFile The DLD blockchain file.
     * @throws IOException If there is a problem reading from the provided file.
     * @throws BlockDecodingException If there is a problem decoding any of the blocks.
     */
    public void loadDldChain(final File dldChainFile) throws IOException, BlockDecodingException {
        if (this.dldChainFile != null) {
            System.out.println("Block chain already loaded - nothing to do.");
            return;
        }

        this.dldChainFile = dldChainFile;

        try (BufferedReader reader = new BufferedReader(new FileReader(dldChainFile))) {
            reader.lines().forEach(line -> {
                try {
                    Block block = objectMapper.readValue(line, Block.class);
                    processNewBlock(block);
                } catch (JsonProcessingException e) {
                    throw new JsonProcessingRuntimeException(e);
                }
            });
        } catch (JsonProcessingRuntimeException e) {
            throw e.getJsonProcessingException();
        }
    }

    /**
     * Write the block to the blockchain file.
     *
     * @param block The block to be written.
     * @throws IOException If there is an issue writing the block to the file.
     */
    public void writeBlock(final Block block) throws IOException {
        try (FileWriter writer = new FileWriter(dldChainFile)) {
            writer.write(objectMapper.writeValueAsString(block));
            writer.write('\n');
        }
    }

    private void processNewBlock(final Block block) {
        this.headBlockSignature = block.getPayloadSignature();
        switch (block.getPayload().getType()) {
            case GENESIS -> {
                Genesis genesisBlock = (Genesis) block.getPayload();
                this.configurationDataStore.handle(genesisBlock);
                this.registrarDataStore.handle(genesisBlock);
            }

            case CREATE_REGISTRAR -> {
                CreateRegistrar createRegistrar = (CreateRegistrar) block.getPayload();
                this.registrarDataStore.handle(createRegistrar);
            }

            case UPDATE_REGISTRAR -> {
                UpdateRegistrar updateRegistrar = (UpdateRegistrar) block.getPayload();
                this.registrarDataStore.handle(updateRegistrar);
            }

            case CREATE_VOTER -> {
                CreateVoter createVoter = (CreateVoter) block.getPayload();
                this.voterDataStore.handle(createVoter);
            }

            case UPDATE_VOTER -> {
                UpdateVoter updateVoter = (UpdateVoter) block.getPayload();
                this.voterDataStore.handle(updateVoter);
            }

            case CREATE_TABULATOR -> {
                CreateTabulator createTabulator = (CreateTabulator) block.getPayload();
                this.tabulatorDataStore.handle(createTabulator);
            }

            case UPDATE_TABULATOR -> {
                UpdateTabulator updateTabulator = (UpdateTabulator) block.getPayload();
                this.tabulatorDataStore.handle(updateTabulator);
            }

            case PROPOSE_LEGISLATION -> {
                ProposeLegislation proposeLegislation = (ProposeLegislation) block.getPayload();
                this.democracyDataStore.handle(proposeLegislation, block.getPayloadSignature());
            }

            case DELEGATE_VOTE -> {
                DelegateVote delegateVote = (DelegateVote) block.getPayload();
                this.democracyDataStore.handle(delegateVote);
            }

            case VOTE -> {
                VotePayload votePayload = (VotePayload) block.getPayload();
                this.democracyDataStore.handle(votePayload);
            }

            case VOTE_TALLY -> {
                VoteTallyPayload voteTallyPayload = (VoteTallyPayload) block.getPayload();
                this.democracyDataStore.handle(voteTallyPayload);
            }

            case VOTE_RESULT -> {
                VoteResultPayload voteResultPayload = (VoteResultPayload) block.getPayload();
                this.democracyDataStore.handle(voteResultPayload);
            }

            case UPDATE_SPEC_VERSION -> {
                UpdateSpecVersion updateSpecVersion = (UpdateSpecVersion) block.getPayload();
                this.configurationDataStore.handle(updateSpecVersion);
            }
        }
    }

    public boolean dldChainFileExists() {
        return dldChainFile == null || dldChainFile.exists();
    }

    public String getHeadBlockSignature() {
        return headBlockSignature;
    }

    public ConfigurationDataStore getConfigurationDataStore() {
        return configurationDataStore;
    }

    public RegistrarDataStore getRegistrarDataStore() {
        return registrarDataStore;
    }

    public TabulatorDataStore getTabulatorDataStore() {
        return tabulatorDataStore;
    }

    public VoterDataStore getVoterDataStore() {
        return voterDataStore;
    }

    public DemocracyDataStore getDemocracyDataStore() {
        return democracyDataStore;
    }

    private static class JsonProcessingRuntimeException extends RuntimeException {

        private final JsonProcessingException jsonProcessingException;

        public JsonProcessingRuntimeException(final JsonProcessingException jsonProcessingException) {
            this.jsonProcessingException = jsonProcessingException;
        }

        public JsonProcessingException getJsonProcessingException() {
            return jsonProcessingException;
        }
    }
}
