package org.dld.app.util;

import org.dld.app.core.ErrorCodes;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

public class GitUtil {

    private GitUtil() {
        throw new RuntimeException("Do not instantiate.");
    }

    /**
     * Fetch the git Repository object associated with the file parameter.
     *
     * @param file The file where the git repository is located.
     * @return An Optional containing the git repository if found.
     */
    public static Optional<Repository> getGitRepo(final File file) {
        try {
            return Optional.of(new FileRepositoryBuilder().setGitDir(file)
                    .setMustExist(true)
                    .build());

        } catch (IOException e) {
            System.err.println("Could not open git repository: " + e.getLocalizedMessage());
            return Optional.empty();
        }
    }

    /**
     * Retrieve a ref from the git repository specified by the objectName parameter.
     *
     * @param repository The git repository.
     * @param objectName The object name to retrieve from the repository.
     * @return An Optional containing the Ref object if found.
     */
    public static Optional<Ref> getRefObject(final Repository repository, final String objectName) {
        try {
            return Optional.ofNullable(repository.exactRef(objectName));
        } catch (IOException e) {
            System.err.println("Error retrieving git ref: " + e.getLocalizedMessage());
            return Optional.empty();
        }
    }
}
