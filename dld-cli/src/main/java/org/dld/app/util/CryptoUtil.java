package org.dld.app.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

public class CryptoUtil {
    private static final String RSA_ALGORITHM = "RSA";
    private static final String SHA256_SIGNATURE_ALGORITHM = "SHA256withRSA";
    private static final int KEY_SIZE = 2048;

    private CryptoUtil() {
        throw new RuntimeException("Do not instantiate.");
    }

    /**
     * Generate a public/private key pair using the RSA algorithm with a key size of 2048.
     *
     * @return A KeyPair generated as per above.
     * @throws NoSuchAlgorithmException if the RSA algorithm can't be found.
     */
    public static KeyPair generateKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator kpg = KeyPairGenerator.getInstance(RSA_ALGORITHM);
        kpg.initialize(KEY_SIZE);
        return kpg.generateKeyPair();
    }

    /**
     * Load an X509-encoded public key from a file.
     *
     * @param file The public key file.
     * @return An Optional containing the key if it could be loaded.
     * @throws NoSuchAlgorithmException If the RSA algorithm cannot be found
     */
    public static Optional<PublicKey> loadPublicKey(final File file) throws NoSuchAlgorithmException {

        try (FileInputStream in = new FileInputStream(file)) {
            byte[] bytes = in.readAllBytes();

            KeyFactory keyFactory = KeyFactory.getInstance(RSA_ALGORITHM);
            X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(bytes);
            PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);

            return Optional.of(publicKey);
        } catch (FileNotFoundException e) {
            System.err.println("Public key file not found.");
        } catch (IOException e) {
            System.err.println("An error occurred while reading the public key file: " + e.getLocalizedMessage());
        } catch (InvalidKeySpecException e) {
            System.err.println("Unable to produce public key from the provided file");
        }
        return Optional.empty();
    }

    /**
     * Load a PKCS8-encoded private key from a file.
     *
     * @param file The private key file.
     * @return An Optional containing the key if it could be loaded.
     * @throws NoSuchAlgorithmException If the RSA algorithm cannot be found
     */
    public static Optional<PrivateKey> loadPrivateKey(final File file) throws NoSuchAlgorithmException {

        try (FileInputStream in = new FileInputStream(file)) {
            byte[] bytes = in.readAllBytes();

            KeyFactory keyFactory = KeyFactory.getInstance(RSA_ALGORITHM);

            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(bytes);
            PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);

            return Optional.of(privateKey);
        } catch (FileNotFoundException e) {
            System.err.println("Private key file not found.");
        } catch (IOException e) {
            System.err.println("An error occurred while reading the private key file: " + e.getLocalizedMessage());
        } catch (InvalidKeySpecException e) {
            System.err.println("Unable to produce private key from the provided file");
        }
        return Optional.empty();
    }

    /**
     * Write the public key to a file.
     *
     * @param publicKey The PublicKey to be written.
     * @param fileName The name of the file to write the key to.
     * @throws IOException If there is a problem writing the key to the specified file.
     */
    public static void writePublicKey(final PublicKey publicKey, final String fileName) throws IOException {
        X509EncodedKeySpec encodedKey = new X509EncodedKeySpec(publicKey.getEncoded());

        try (FileOutputStream out = new FileOutputStream(fileName)) {
            out.write(encodedKey.getEncoded());
        }
    }

    /**
     * Write the private key to a file.
     *
     * @param privateKey The PrivateKey to be written.
     * @param fileName The name of the file to write the key to.
     * @throws IOException If there is a problem writing the key to the specified file.
     */
    public static void writePrivateKey(final PrivateKey privateKey, final String fileName) throws IOException {
        PKCS8EncodedKeySpec encodedKey = new PKCS8EncodedKeySpec(privateKey.getEncoded());

        try (FileOutputStream out = new FileOutputStream(fileName)) {
            out.write(encodedKey.getEncoded());
        }
    }

    /**
     * Convert a PublicKey to a Base64-encoded String.
     *
     * @param publicKey The PublicKey to be encoded.
     * @return The Base64 String representation of the PublicKey.
     */
    public static String encodePublicKeyToString(final PublicKey publicKey) {
        return Base64.getEncoder().encodeToString(publicKey.getEncoded());
    }

    /**
     * Decode a PublicKey from a Base64 string.
     *
     * @param publicKeyString The Base64-encoded public key string.
     * @return An Optional containing the PublicKey if it was possible to decode it.
     */
    public static Optional<PublicKey> decodePublicKeyFromString(final String publicKeyString) {
        if (publicKeyString == null) {
            return Optional.empty();
        }

        try {
            byte[] decodedString = Base64.getDecoder().decode(publicKeyString);
            X509EncodedKeySpec x509PublicKey = new X509EncodedKeySpec(decodedString);
            KeyFactory keyFactory = KeyFactory.getInstance(RSA_ALGORITHM);

            return Optional.of(keyFactory.generatePublic(x509PublicKey));
        } catch (NoSuchAlgorithmException e) {
            System.err.println("Unable to find " + RSA_ALGORITHM + "." + e.getLocalizedMessage());
        } catch (InvalidKeySpecException e) {
            System.err.println("Unable to decode public key." + e.getLocalizedMessage());
        }

        return Optional.empty();
    }

    /**
     * Validate that a public/private key pair match.
     *
     * @param privateKey The PrivateKey to be checked.
     * @param publicKey The PublicKey to be checked.
     * @return True if the keys match, false otherwise.
     * @throws NoSuchAlgorithmException If the SHA-256 with RSA algorithm can't be found
     */
    public static boolean doesKeyPairMatch(final PrivateKey privateKey, final PublicKey publicKey)
            throws NoSuchAlgorithmException {
        // Create a challenge
        byte[] challenge = new byte[10000];
        ThreadLocalRandom.current().nextBytes(challenge);

        // sign using the private key
        Signature signer = Signature.getInstance(SHA256_SIGNATURE_ALGORITHM);

        try {
            signer.initSign(privateKey);
            signer.update(challenge);
            byte[] signature = signer.sign();

            // verify signature using the public key
            signer.initVerify(publicKey);
            signer.update(challenge);

            return signer.verify(signature);
        } catch (InvalidKeyException e) {
            System.err.println("Invalid key: " + e.getLocalizedMessage());
        } catch (SignatureException e) {
            System.err.println("Signature incorrectly initialized: " + e.getLocalizedMessage());
        }

        return false;
    }

    /**
     * Sign a payload using a PrivateKey.
     *
     * @param payload The payload to be signed.
     * @param privateKey The PrivateKey to use to sign the payload.
     * @return The Signature of the payload encoded in Base64 as a String.
     * @throws NoSuchAlgorithmException If the SHA-256 with RSA algorithm can't be found
     */
    public static Optional<String> sign(final String payload, final PrivateKey privateKey) throws NoSuchAlgorithmException {
        Signature signature = Signature.getInstance(SHA256_SIGNATURE_ALGORITHM);
        try {
            signature.initSign(privateKey);
            signature.update(payload.getBytes(StandardCharsets.UTF_8));
            byte[] signedBytes = signature.sign();
            return Optional.of(Base64.getEncoder().encodeToString(signedBytes));
        } catch (InvalidKeyException | SignatureException e) {
            System.err.println("Failed to sign the payload: " + e.getLocalizedMessage());
            return Optional.empty();
        }
    }

    /**
     * Verify that a signature matches the payload signed by the private key associated with
     * the public key parameter.
     *
     * @param base64EncodedSignature The Base64-encoded signature as a String.
     * @param payload The payload JSON string.
     * @param publicKey The PublicKey to use for signature verification.
     * @return True if the payload matches the provided signature and is signed by the PrivateKey
     * associated with the public key passed to this method.
     * @throws NoSuchAlgorithmException If the SHA-256 with RSA algorithm can't be found
     */
    public static boolean verifySignature(final String base64EncodedSignature, final String payload,
                                          final PublicKey publicKey) throws NoSuchAlgorithmException {
        Signature sig = Signature.getInstance(SHA256_SIGNATURE_ALGORITHM);
        try {
            sig.initVerify(publicKey);
            sig.update(payload.getBytes(StandardCharsets.UTF_8));

            return sig.verify(Base64.getDecoder().decode(base64EncodedSignature));
        } catch (InvalidKeyException | SignatureException e) {
            System.err.println("Signature verification failed: " + e.getLocalizedMessage());
            return false;
        }
    }
}
