package org.dld.chain.block;

public class Genesis extends BlockPayload {

    private String legislationRepositorySourceURI;
    private String legislationRepositoryMainlineBranchName;
    private String legislationRepositoryMainlineHeadHash;
    private String genesisSuperRegistrarPublicKey;
    private String genesisSuperRegistrarId;
    private String specVersion;

    public Genesis() {
        super(BlockTypes.GENESIS);
    }

    public String getLegislationRepositorySourceURI() {
        return legislationRepositorySourceURI;
    }

    public Genesis setLegislationRepositorySourceURI(final String legislationRepositorySourceURI) {
        this.legislationRepositorySourceURI = legislationRepositorySourceURI;
        return this;
    }

    public String getLegislationRepositoryMainlineBranchName() {
        return legislationRepositoryMainlineBranchName;
    }

    public Genesis setLegislationRepositoryMainlineBranchName(final String legislationRepositoryMainlineBranchName) {
        this.legislationRepositoryMainlineBranchName = legislationRepositoryMainlineBranchName;
        return this;
    }

    public String getLegislationRepositoryMainlineHeadHash() {
        return legislationRepositoryMainlineHeadHash;
    }

    public Genesis setLegislationRepositoryMainlineHeadHash(final String legislationRepositoryMainlineHeadHash) {
        this.legislationRepositoryMainlineHeadHash = legislationRepositoryMainlineHeadHash;
        return this;
    }

    public String getGenesisSuperRegistrarPublicKey() {
        return genesisSuperRegistrarPublicKey;
    }

    public Genesis setGenesisSuperRegistrarPublicKey(final String genesisSuperRegistrarPublicKey) {
        this.genesisSuperRegistrarPublicKey = genesisSuperRegistrarPublicKey;
        return this;
    }

    public String getGenesisSuperRegistrarId() {
        return genesisSuperRegistrarId;
    }

    public Genesis setGenesisSuperRegistrarId(final String genesisSuperRegistrarId) {
        this.genesisSuperRegistrarId = genesisSuperRegistrarId;
        return this;
    }

    public String getSpecVersion() {
        return specVersion;
    }

    public Genesis setSpecVersion(final String specVersion) {
        this.specVersion = specVersion;
        return this;
    }

    @Override
    public String toString() {
        return "Genesis{" +
                "type=" + type +
                ", parentBlockSignature='" + parentBlockSignature + '\'' +
                ", legislationRepositorySourceURI='" + legislationRepositorySourceURI + '\'' +
                ", legislationRepositoryMainlineBranchName='" + legislationRepositoryMainlineBranchName + '\'' +
                ", legislationRepositoryMainlineHeadHash='" + legislationRepositoryMainlineHeadHash + '\'' +
                ", genesisSuperRegistrarPublicKey='" + genesisSuperRegistrarPublicKey + '\'' +
                ", genesisSuperRegistrarId='" + genesisSuperRegistrarId + '\'' +
                ", specVersion='" + specVersion + '\'' +
                '}';
    }
}
