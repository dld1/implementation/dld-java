package org.dld.chain.block;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.ZonedDateTime;

public class UpdateRegistrar extends BlockPayload {

    private String registrarId;
    private String registrarPublicKey;
    private boolean isSuperRegistrar;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private ZonedDateTime expires;

    public UpdateRegistrar() {
        super(BlockTypes.UPDATE_REGISTRAR);
    }

    public String getRegistrarId() {
        return registrarId;
    }

    public UpdateRegistrar setRegistrarId(final String registrarId) {
        this.registrarId = registrarId;
        return this;
    }

    public String getRegistrarPublicKey() {
        return registrarPublicKey;
    }

    public UpdateRegistrar setRegistrarPublicKey(final String registrarPublicKey) {
        this.registrarPublicKey = registrarPublicKey;
        return this;
    }

    public boolean isSuperRegistrar() {
        return isSuperRegistrar;
    }

    public UpdateRegistrar setSuperRegistrar(final boolean superRegistrar) {
        isSuperRegistrar = superRegistrar;
        return this;
    }

    public ZonedDateTime getExpires() {
        return expires;
    }

    public UpdateRegistrar setExpires(final ZonedDateTime expires) {
        this.expires = expires;
        return this;
    }

    @Override
    public String toString() {
        return "UpdateRegistrar{" +
                "type=" + type +
                ", parentBlockSignature='" + parentBlockSignature + '\'' +
                ", registrarId='" + registrarId + '\'' +
                ", registrarPublicKey='" + registrarPublicKey + '\'' +
                ", isSuperRegistrar=" + isSuperRegistrar +
                ", expires=" + expires +
                '}';
    }
}
