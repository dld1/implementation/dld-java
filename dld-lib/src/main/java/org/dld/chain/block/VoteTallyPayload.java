package org.dld.chain.block;

public class VoteTallyPayload extends BlockPayload {

    private String tabulatorId;
    private String proposalSignature;
    private long totalVotes;
    private long totalDirectVotes;
    private long totalInFavour;
    private long totalOpposed;
    private boolean proposalPassed;
    private String inFavourChecksum;
    private String opposedChecksum;

    public VoteTallyPayload() {
        super(BlockTypes.VOTE_TALLY);
    }

    public String getTabulatorId() {
        return tabulatorId;
    }

    public VoteTallyPayload setTabulatorId(final String tabulatorId) {
        this.tabulatorId = tabulatorId;
        return this;
    }

    public String getProposalSignature() {
        return proposalSignature;
    }

    public VoteTallyPayload setProposalSignature(final String proposalSignature) {
        this.proposalSignature = proposalSignature;
        return this;
    }

    public long getTotalVotes() {
        return totalVotes;
    }

    public VoteTallyPayload setTotalVotes(final long totalVotes) {
        this.totalVotes = totalVotes;
        return this;
    }

    public long getTotalDirectVotes() {
        return totalDirectVotes;
    }

    public VoteTallyPayload setTotalDirectVotes(final long totalDirectVotes) {
        this.totalDirectVotes = totalDirectVotes;
        return this;
    }

    public long getTotalInFavour() {
        return totalInFavour;
    }

    public VoteTallyPayload setTotalInFavour(final long totalInFavour) {
        this.totalInFavour = totalInFavour;
        return this;
    }

    public long getTotalOpposed() {
        return totalOpposed;
    }

    public VoteTallyPayload setTotalOpposed(final long totalOpposed) {
        this.totalOpposed = totalOpposed;
        return this;
    }

    public boolean isProposalPassed() {
        return proposalPassed;
    }

    public VoteTallyPayload setProposalPassed(final boolean proposalPassed) {
        this.proposalPassed = proposalPassed;
        return this;
    }

    public String getInFavourChecksum() {
        return inFavourChecksum;
    }

    public VoteTallyPayload setInFavourChecksum(final String inFavourChecksum) {
        this.inFavourChecksum = inFavourChecksum;
        return this;
    }

    public String getOpposedChecksum() {
        return opposedChecksum;
    }

    public VoteTallyPayload setOpposedChecksum(final String opposedChecksum) {
        this.opposedChecksum = opposedChecksum;
        return this;
    }

    @Override
    public String toString() {
        return "VoteTally{" +
                "type=" + type +
                ", parentBlockSignature='" + parentBlockSignature + '\'' +
                ", tabulatorId='" + tabulatorId + '\'' +
                ", proposalSignature='" + proposalSignature + '\'' +
                ", totalVotes=" + totalVotes +
                ", totalDirectVotes=" + totalDirectVotes +
                ", totalInFavour=" + totalInFavour +
                ", totalOpposed=" + totalOpposed +
                ", proposalPassed=" + proposalPassed +
                ", inFavourChecksum='" + inFavourChecksum + '\'' +
                ", opposedChecksum='" + opposedChecksum + '\'' +
                '}';
    }
}
