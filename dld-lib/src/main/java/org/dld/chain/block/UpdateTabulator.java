package org.dld.chain.block;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.ZonedDateTime;

public class UpdateTabulator extends BlockPayload {

    private String tabulatorId;
    private String tabulatorPublicKey;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private ZonedDateTime expires;

    public UpdateTabulator() {
        super(BlockTypes.UPDATE_TABULATOR);
    }

    public String getTabulatorId() {
        return tabulatorId;
    }

    public UpdateTabulator setTabulatorId(final String tabulatorId) {
        this.tabulatorId = tabulatorId;
        return this;
    }

    public String getTabulatorPublicKey() {
        return tabulatorPublicKey;
    }

    public UpdateTabulator setTabulatorPublicKey(final String tabulatorPublicKey) {
        this.tabulatorPublicKey = tabulatorPublicKey;
        return this;
    }

    public ZonedDateTime getExpires() {
        return expires;
    }

    public UpdateTabulator setExpires(final ZonedDateTime expires) {
        this.expires = expires;
        return this;
    }

    @Override
    public String toString() {
        return "UpdateTabulator{" +
                "type=" + type +
                ", parentBlockSignature='" + parentBlockSignature + '\'' +
                ", tabulatorId='" + tabulatorId + '\'' +
                ", tabulatorPublicKey='" + tabulatorPublicKey + '\'' +
                ", expires=" + expires +
                '}';
    }
}
