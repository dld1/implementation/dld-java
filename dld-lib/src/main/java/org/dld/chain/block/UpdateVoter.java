package org.dld.chain.block;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.ZonedDateTime;

public class UpdateVoter extends BlockPayload {

    private String voterId;
    private String voterPublicKey;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private ZonedDateTime expires;

    public UpdateVoter() {
        super(BlockTypes.UPDATE_VOTER);
    }

    public String getVoterId() {
        return voterId;
    }

    public UpdateVoter setVoterId(final String voterId) {
        this.voterId = voterId;
        return this;
    }

    public String getVoterPublicKey() {
        return voterPublicKey;
    }

    public UpdateVoter setVoterPublicKey(final String voterPublicKey) {
        this.voterPublicKey = voterPublicKey;
        return this;
    }

    public ZonedDateTime getExpires() {
        return expires;
    }

    public UpdateVoter setExpires(final ZonedDateTime expires) {
        this.expires = expires;
        return this;
    }

    @Override
    public String toString() {
        return "UpdateVoter{" +
                "type=" + type +
                ", parentBlockSignature='" + parentBlockSignature + '\'' +
                ", voterId='" + voterId + '\'' +
                ", voterPublicKey='" + voterPublicKey + '\'' +
                ", expires='" + expires + '\'' +
                '}';
    }
}
