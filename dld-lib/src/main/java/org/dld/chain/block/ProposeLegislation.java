package org.dld.chain.block;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.ZonedDateTime;

public class ProposeLegislation extends BlockPayload {

    private String voterId;
    private String legislationProposalSourceURI;
    private String legislationProposalBranchName;
    private String legislationProposalHeadHash;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private ZonedDateTime voteOpenTime;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private ZonedDateTime voteCloseTime;

    public ProposeLegislation() {
        super(BlockTypes.PROPOSE_LEGISLATION);
    }

    public String getVoterId() {
        return voterId;
    }

    public ProposeLegislation setVoterId(final String voterId) {
        this.voterId = voterId;
        return this;
    }

    public String getLegislationProposalSourceURI() {
        return legislationProposalSourceURI;
    }

    public ProposeLegislation setLegislationProposalSourceURI(final String legislationProposalSourceURI) {
        this.legislationProposalSourceURI = legislationProposalSourceURI;
        return this;
    }

    public String getLegislationProposalBranchName() {
        return legislationProposalBranchName;
    }

    public ProposeLegislation setLegislationProposalBranchName(final String legislationProposalBranchName) {
        this.legislationProposalBranchName = legislationProposalBranchName;
        return this;
    }

    public String getLegislationProposalHeadHash() {
        return legislationProposalHeadHash;
    }

    public ProposeLegislation setLegislationProposalHeadHash(final String legislationProposalHeadHash) {
        this.legislationProposalHeadHash = legislationProposalHeadHash;
        return this;
    }

    public ZonedDateTime getVoteOpenTime() {
        return voteOpenTime;
    }

    public ProposeLegislation setVoteOpenTime(final ZonedDateTime voteOpenTime) {
        this.voteOpenTime = voteOpenTime;
        return this;
    }

    public ZonedDateTime getVoteCloseTime() {
        return voteCloseTime;
    }

    public ProposeLegislation setVoteCloseTime(final ZonedDateTime voteCloseTime) {
        this.voteCloseTime = voteCloseTime;
        return this;
    }

    @Override
    public String toString() {
        return "ProposeLegislation{" +
                "type=" + type +
                ", parentBlockSignature='" + parentBlockSignature + '\'' +
                ", voterId='" + voterId + '\'' +
                ", legislationProposalSourceURI='" + legislationProposalSourceURI + '\'' +
                ", legislationProposalBranchName='" + legislationProposalBranchName + '\'' +
                ", legislationProposalHeadHash='" + legislationProposalHeadHash + '\'' +
                ", voteOpenTime=" + voteOpenTime +
                ", voteCloseTime=" + voteCloseTime +
                '}';
    }
}
