package org.dld.chain.block;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.ZonedDateTime;

public class CreateRegistrar extends BlockPayload {

    private String registrarId;
    private String registrarPublicKey;
    private boolean isSuperRegistrar;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private ZonedDateTime expires;

    public CreateRegistrar() {
        super(BlockTypes.CREATE_REGISTRAR);
    }

    public String getRegistrarId() {
        return registrarId;
    }

    public CreateRegistrar setRegistrarId(final String registrarId) {
        this.registrarId = registrarId;
        return this;
    }

    public String getRegistrarPublicKey() {
        return registrarPublicKey;
    }

    public CreateRegistrar setRegistrarPublicKey(final String registrarPublicKey) {
        this.registrarPublicKey = registrarPublicKey;
        return this;
    }

    public boolean isSuperRegistrar() {
        return isSuperRegistrar;
    }

    public CreateRegistrar setSuperRegistrar(final boolean superRegistrar) {
        isSuperRegistrar = superRegistrar;
        return this;
    }

    public ZonedDateTime getExpires() {
        return expires;
    }

    public CreateRegistrar setExpires(final ZonedDateTime expires) {
        this.expires = expires;
        return this;
    }

    public CreateRegistrar setParentBlockSignature(final String parentBlockSignature) {
        super.setParentBlockSignature(parentBlockSignature);
        return this;
    }

    @Override
    public String toString() {
        return "CreateRegistrar{" +
                "type=" + type +
                ", parentBlockSignature='" + parentBlockSignature + '\'' +
                ", registrarId='" + registrarId + '\'' +
                ", registrarPublicKey='" + registrarPublicKey + '\'' +
                ", isSuperRegistrar=" + isSuperRegistrar +
                ", expires=" + expires +
                '}';
    }
}
